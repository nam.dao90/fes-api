FROM node:lts-alpine3.14
RUN apk --no-cache add python3
RUN npm config set python /usr/bin/python
WORKDIR /app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /app
COPY tsconfig.json /app

RUN npm install
# RUN npm run install:pm2

COPY . /app
ENV PORT 3000
EXPOSE $PORT
CMD [ "npm", "run", "dev"]
