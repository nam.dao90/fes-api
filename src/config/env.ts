import dotenv from 'dotenv';
import Joi, { ValidationResult } from 'joi';
import path from 'path';

dotenv.config({ path: path.join(__dirname, '../../.env') });

type envApp = 'development' | 'stag' | 'production';
type configApp = {
  port: number;
  portRedis: number;
  env: envApp;
  postgres: {
    DB_HOSTNAME: string;
    DB_PASSWORD: string;
    DB_USER: string;
    DB_NAME: string;
  };
  jwt: {
    secret: string;
    accessExpirationMinutes: number;
    refreshExpirationDays: number;
    resetPasswordExpirationMinutes: number;
    verifyEmailExpirationMinutes: number;
  };
};

interface IEnvConfigResult extends ValidationResult {
  value: {
    NODE_ENV: envApp;
    PORT: number;
    PORT_REDIS: number;
    DB_HOSTNAME: string;
    DB_PASSWORD: string;
    DB_USER: string;
    DB_NAME: string;
    JWT_SECRET: string;
    JWT_ACCESS_EXPIRATION_MINUTES: number;
    JWT_REFRESH_EXPIRATION_DAYS: number;
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: number;
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: number;
  };
}

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string()
      .valid('production', 'dev', 'stag')
      .required(),
    PORT: Joi.number().default(process.env.PORT),
    PORT_REDIS: Joi.number().default(process.env.PORT_REDIS),
    DB_HOSTNAME: Joi.string()
      .required()
      .default(process.env.DB_HOSTNAME)
      .description('Host name postgres'),
    DB_PASSWORD: Joi.string()
      .required()
      .default(process.env.DB_PASSWORD)
      .description('Password user name postgres'),
    DB_USER: Joi.string()
      .required()
      .default(process.env.DB_USER)
      .description('User name postgres'),
    DB_NAME: Joi.string()
      .required()
      .default(process.env.DB_NAME)
      .description('Database name postgres'),
    JWT_SECRET: Joi.string()
      .required()
      .default(process.env.JWT_SECRET)
      .description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number()
      .default(60)
      .description('minutes after which access tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number()
      .default(60)
      .description('days after which refresh tokens expire'),
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description(
        'minutes after which reset password token expires'
      ),
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description('minutes after which verify email token expires'),
  })
  .unknown();

const { value: envVal, error } = envVarsSchema
  .prefs({ errors: { label: 'key' } })
  .validate(process.env) as IEnvConfigResult;

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}
const configEnv: configApp = {
  port: envVal.PORT,
  portRedis: envVal.PORT_REDIS,
  env: envVal.NODE_ENV,
  postgres: {
    DB_HOSTNAME: envVal.DB_HOSTNAME,
    DB_NAME: envVal.DB_NAME,
    DB_USER: envVal.DB_USER,
    DB_PASSWORD: envVal.DB_PASSWORD,
  },
  jwt: {
    secret: envVal.JWT_SECRET,
    accessExpirationMinutes: envVal.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVal.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes:
      envVal.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes:
      envVal.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
  },
};
export default configEnv;
