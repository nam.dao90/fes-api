import { createClient } from 'redis';
import { logger } from './logger';

const redisClient = createClient();
const connect = async () => {
  redisClient.on('error', err =>
    logger.error('Redis client error:', err)
  );
  return redisClient.connect();
};

const setValue = (key: string, value: string) => {
  redisClient.set(key, value);
};
const getValue = async (key: string) => {
  const valueRedis = await redisClient.get(key);
  return valueRedis;
};

const setValueExpire = (key: string, value: string, time: number) => {
  redisClient.setEx(key, time, value);
};

const redisDb = {
  redisClient,
  connect,
  getValue,
  setValue,
  setValueExpire,
};
export default redisDb;
