export const ROUTE_CONSTANT = {
  AUTH: {
    REGISTER: '/register',
    LOGIN: '/login',
    LOG_OUT: '/logout',
    REFRESH_TOKENS: '/refresh-tokens',
    FORGOT_PASSWORD: '/forgot-password',
    RESET_PASSWORD: '/reset-password',
    SEND_VERIFY_EMAIL: '/send-verification-email',
    VERIFY_EMAIL: '/verify-email',
  },
  USERS: {
    DEFAULT: '/',
  },
  ROLES: {
    DEFAULT: '/',
    DETAIL: '/:roleName',
  },
  MODULES: {
    DEFAULT: '/',
    DETAIL: '/:moduleName',
  },
  PERMISSION: {
    DEFAULT: '/',
  },
};
export const ROUTE_GROUP = {
  AUTH: '/auth',
  USERS: '/users',
  ROLES: '/roles',
  MODULES: '/modules',
  PERMISSION: '/permission',
};
