import httpStatus from 'http-status';

const resStatus = (statusCode: number, success: boolean) => {
  return {
    code: statusCode,
    success,
  };
};

export const resStatusSuccess = () => resStatus(httpStatus.OK, true);

export const resStatusInputFailure = () =>
  resStatus(httpStatus.UNPROCESSABLE_ENTITY, false);

export const resStatusInternalSever = () =>
  resStatus(httpStatus.INTERNAL_SERVER_ERROR, false);

export const resStatusUnAuthorize = () =>
  resStatus(httpStatus.UNAUTHORIZED, false);

export const resStatusAccessDenied = () =>
  resStatus(httpStatus.FORBIDDEN, false);

export const resStatusBadRequest = () =>
  resStatus(httpStatus.BAD_REQUEST, false);
export const resStatusNotFound = () =>
  resStatus(httpStatus.NOT_FOUND, false);
