import {
  AbilityBuilder,
  Ability,
  ForcedSubject,
} from '@casl/ability';
import { RoleAttribute } from '@src/modules/roles/types/model';

export type IActionTypes =
  | 'manage'
  | 'read'
  | 'canAdd'
  | 'canView'
  | 'canUpdate'
  | 'canSoftDelete'
  | 'canList';

/** Add others modules into database */
export type IModuleNames =
  | 'users'
  | 'stock'
  | 'orders'
  | 'roles'
  | 'all';

export type IRoleSubject = RoleAttribute &
  ForcedSubject<IModuleNames>;
type AbilityPermissionRole = Ability<
  [IActionTypes, IModuleNames | IRoleSubject]
>;
export default function AbilityApp(
  module: IModuleNames,
  userRole: IRoleSubject
) {
  const { can, cannot, build } =
    new AbilityBuilder<AbilityPermissionRole>(Ability);

  if (userRole.role === 'sa') {
    can('manage', 'all'); // read-write access to everything
  }
  if (userRole.role === 'admin') {
    switch (module) {
      case 'users': {
        can('canAdd', module);
        can('canList', module);
        can('canUpdate', module);
        cannot('canSoftDelete', module);
        break;
      }
      default: {
        cannot('manage', 'all');
        break;
      }
    }
  }
  if (userRole.role === 'member') {
    switch (module) {
      case 'roles': {
        can('canView', module);
        can('canUpdate', module);
        cannot('canAdd', module);
        cannot('canList', module);
        cannot('canSoftDelete', module);
        break;
      }
      case 'users': {
        can('canView', module);
        can('canUpdate', module);
        cannot('canAdd', module);
        cannot('canList', module);
        cannot('canSoftDelete', module);
        break;
      }
      default: {
        cannot('manage', 'all');
        break;
      }
    }
  }

  return build();
}
