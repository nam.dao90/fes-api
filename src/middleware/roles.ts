import { NextFunction, Request, Response } from 'express';
import { subject } from '@casl/ability';
import { internalException } from '@utils/exception';
import { accessDeniedUser } from '@utils/apiErrorInput';
import {
  UserAttributes,
  UserInstance,
} from '@modules/user/types/model';
import AbilityApp, {
  IActionTypes,
  IModuleNames,
  IRoleSubject,
} from './acl/ability';

const accessRoleByModule =
  (role: IActionTypes, modules: IModuleNames) =>
  async (
    _req: Request,
    res: Response & { locals: { user: UserInstance } },
    next: NextFunction
  ) => {
    try {
      const { user } = res.locals;
      const userRole = user?.role as IRoleSubject;
      if (!userRole) {
        return res.json(accessDeniedUser());
      }
      const objModule = subject(modules, userRole);
      const hasRollAccess = AbilityApp(modules, userRole).can(
        role,
        objModule
      );
      if (!hasRollAccess) return res.json(accessDeniedUser());
      next();
    } catch (err) {
      internalException(res, err);
    }
  };

const accessRolesBase =
  (role: IActionTypes) =>
  async (
    _req: Request,
    res: Response & { locals: { user: UserAttributes } },
    next: NextFunction
  ) => {
    try {
      const { user } = res.locals;
      // const userRole = {
      //   name: user.role[0].name,
      // };
      // const userRole = user.role.find(
      //   r => r.name === 'admin' || r.name === 'super_admin'
      // );
      // if (!userRole) {
      //   return res.json(accessDeniedUser());
      // }
      // if (
      //   role === 'canSoftDelete' &&
      //   userRole.name !== 'admin' &&
      //   userRole.name !== 'super_admin'
      // ) {
      //   return res.json(accessDeniedUser());
      // }
      next();
    } catch (err) {
      internalException(res, err);
    }
  };

const roles = {
  accessRoleByModule,
  accessRolesBase,
};

export default roles;
