import express from 'express';
import { ROUTE_CONSTANT } from '@routes/constant';
import validateRequest from '@middleware/validateSchema';
import authMiddleWare from '@middleware/auth';
import authController from './controller';
import authValidation from './validation';

const router = express.Router();

router.post(
  ROUTE_CONSTANT.AUTH.REGISTER,
  validateRequest(authValidation.register),
  authController.register
);
router.post(
  ROUTE_CONSTANT.AUTH.LOGIN,
  validateRequest(authValidation.login),
  authController.login
);
router.post(
  ROUTE_CONSTANT.AUTH.LOG_OUT,
  authMiddleWare.authJwt,
  authController.logout
);
router.post(
  ROUTE_CONSTANT.AUTH.REFRESH_TOKENS,
  validateRequest(authValidation.refreshTokens),
  authController.refreshToken
);

export default router;
