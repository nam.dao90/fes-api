import {
  UserAttributes,
  UserInstance,
} from '@src/modules/user/types/model';
import { Model, Optional } from 'sequelize';

export type ITokenTypes =
  | 'access'
  | 'refresh'
  | 'resetPassword'
  | 'verifyEmail';
export enum ETokenTypes {
  ACCESS = 'access',
  REFRESH = 'refresh',
  RESET_PASSWORD = 'resetPassword',
  VERIFY_EMAIL = 'verifyEmail',
}
export interface TokenAttribute {
  id?: number;
  userId: string;
  blacklisted: boolean;
  token: string;
  refreshToken: string;
  type: ITokenTypes;
  expires: Date;
  deletedAt?: Date;
}
export type TokenCreationAttributes = Optional<
  TokenAttribute,
  'blacklisted'
>;

export interface TokenInstance
  extends Model<TokenCreationAttributes, TokenAttribute>,
    TokenAttribute {
  /**
   * Read more https://sequelize.org/master/manual/assocs.html
   */
  getUser?: () => Promise<UserInstance>;
  createUser?: (user: UserAttributes) => Promise<UserInstance>;
}
