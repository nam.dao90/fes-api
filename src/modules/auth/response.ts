import { resStatusSuccess } from '@utils/status';
import { IClientResponse } from '@utils/types';
import { IResRegister, IResToken } from './types/controller';
import { UserInstance } from '../user/types/model';

const parseDataRegister = (
  user: UserInstance,
  token: IResToken,
  role = 'member'
): IClientResponse<IResRegister> => {
  return {
    status: resStatusSuccess(),
    data: {
      user: {
        email: user.email,
        isEmailVerified: user.isEmailVerified,
        name: user.name,
        role,
      },
      token,
    },
  };
};

const parseLogout = (message: string) => ({
  status: resStatusSuccess(),
  data: { message },
});

const parseRefreshToken = (
  token: IResToken
): IClientResponse<IResToken> => {
  return {
    status: resStatusSuccess(),
    data: token,
  };
};
const authData = {
  parseDataRegister,
  parseLogout,
  parseRefreshToken,
};

export default authData;
