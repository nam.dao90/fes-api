import { Request, Response } from 'express';
import httpStatus from 'http-status';
import rolesServices from '@modules/roles/services';
import usersServices from '@modules/user/services';
import { internalException } from '@utils/exception';
import { IRawUser } from '@modules/user/types/controller';
import redisDb from '@src/config/redis';
import authServices from './services';
import clientData from './response';
import {
  IRawLogin,
  IRawRefreshToken,
  IRequestData,
} from './types/controller';
import { REDIS_AUTH } from './redis';

const register = async (
  req: IRequestData<IRawUser>,
  res: Response
) => {
  try {
    const { error: errorRole, data: dataRole } =
      await rolesServices.getRoleByName('member');
    if (errorRole) {
      return res.json(errorRole);
    }

    const dataUser: IRawUser = {
      ...req.body,
      idRole: dataRole.id,
    };
    const { error, data } = await usersServices.createUser(dataUser);
    if (error) {
      return res.json(error);
    }
    const tokens = await authServices.generateAuthTokens(data);

    const dataResponseClient = clientData.parseDataRegister(
      data,
      tokens,
      dataRole.role
    );
    const dataRedis = {
      id: data.id,
      email: data.email,
      name: data.name,
      role: dataRole.role,
    };
    redisDb.redisClient.zAdd(REDIS_AUTH.RD_USER_LIST, {
      score: dataRedis.id,
      value: JSON.stringify(dataRedis),
    });
    res.json(dataResponseClient);
  } catch (err) {
    internalException(res, err);
  }
};

const login = async (req: IRequestData<IRawLogin>, res: Response) => {
  try {
    const { data, error } =
      await authServices.loginUserWithEmailAndPassword(
        req.body.email,
        req.body.password
      );
    if (error) {
      return res.json(error);
    }
    const tokens = await authServices.generateAuthTokens(data);
    const roleDf = await data?.getRole();
    const dataResponseClient = clientData.parseDataRegister(
      data,
      tokens,
      roleDf?.role
    );
    res.json(dataResponseClient);
  } catch (err) {
    internalException(res, err);
  }
};

const logout = async (req: Request, res: Response) => {
  try {
    const token =
      (req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'Bearer' &&
        req.headers.authorization.split(' ')[1]) ||
      '';
    const { error, data } = await authServices.logout(token);
    if (error) {
      return res.status(httpStatus.OK).json(error);
    }
    res.json(clientData.parseLogout(data));
  } catch (err) {
    internalException(res, err);
  }
};

const refreshToken = async (
  req: IRequestData<IRawRefreshToken>,
  res: Response
) => {
  try {
    const { error, data } = await authServices.refreshAuth(
      req.body.refreshToken
    );
    if (error) {
      return res.json(error);
    }
    const dataResponseClient = clientData.parseRefreshToken(data);
    res.json(dataResponseClient);
  } catch (err) {
    internalException(res, err);
  }
};
const authController = {
  register,
  login,
  logout,
  refreshToken,
};
export default authController;
