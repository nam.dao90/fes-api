import {
  TokenInstance,
  TokenAttribute,
} from '@src/modules/auth/types/model';
import {
  RoleAttribute,
  RoleInstance,
} from '@src/modules/roles/types/model';
import { Optional, Model } from 'sequelize';

export type UserAttributes = {
  id: number;
  uid: string;
  name: string;
  email: string;
  password: string;
  isEmailVerified?: boolean;
};

export type UserCreationAttributes = Optional<UserAttributes, 'id'>;

export interface UserInstance
  extends Model<UserCreationAttributes, UserAttributes>,
    UserAttributes {
  // Just call it when Query User include RoleModel
  role?: RoleAttribute;
  /**
   * Read more https://sequelize.org/master/manual/assocs.html
   */
  getTokens?: () => Promise<Array<TokenInstance>>;
  getRole: () => Promise<RoleInstance>;
  createToken?: (token: TokenAttribute) => Promise<TokenInstance>;
  countTokens?: () => Promise<number>;
  hasToken?: (id: number) => Promise<boolean>;
  hasTokens?: (ids: Array<number>) => Promise<boolean>;
}
