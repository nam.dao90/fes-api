import User from '@modules/user/model';
import { handleErrorsModel } from '@utils/apiErrorInput';
import { IErrorField, IErrorResponse } from '@utils/types';
import { IRawUser } from './types/controller';
import { UserInstance } from './types/model';
// import { IDocumentUser } from './types/service';

const getUserViaEmail = (email: string) =>
  User.findOne({ where: { email } });

const createUser = async (
  userBody: IRawUser
): Promise<{
  error: IErrorResponse<IErrorField[]> | null;
  data: UserInstance | null;
}> => {
  if (await getUserViaEmail(userBody.email)) {
    const errorResponse = handleErrorsModel({
      key: 'email',
      message: 'Email already taken',
    });
    return { error: errorResponse, data: null };
  }
  const dataUser = await User.create(userBody);

  return { error: null, data: dataUser.toJSON() };
};

const getUserById = (id: string) => User.findByPk(id);
const queryUsers = () => User.findAll();
const usersServices = {
  createUser,
  getUserViaEmail,
  getUserById,
  queryUsers,
};

export default usersServices;
