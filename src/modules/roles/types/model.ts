import {
  UserAttributes,
  UserInstance,
} from '@src/modules/user/types/model';
import { Optional, Model } from 'sequelize';

export type RoleAttribute = {
  id?: number;
  role: string;
  aliasRole: string;
};
type RoleCreationAttributes = Optional<RoleAttribute, 'aliasRole'>;

export interface RoleInstance
  extends Model<RoleCreationAttributes, RoleAttribute>,
    RoleAttribute {
  getUser?: () => Promise<UserInstance>;
  createUser?: (user: UserAttributes) => Promise<UserInstance>;
}
