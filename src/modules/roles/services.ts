import Roles from '@modules/roles/model';
import { badRequest, handleErrorsModel } from '@utils/apiErrorInput';
import { IErrorField, IErrorResponse } from '@utils/types';
import { IRawRole } from './types/controller';
import { RoleInstance } from './types/model';

const getListRole = async () => Roles.findAll();

const getRole = async (role: string) =>
  Roles.findOne({ where: { role } });

const getRoleByName = async (
  name: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: RoleInstance | null;
}> => {
  try {
    const existRole = await getRole(name);
    if (!existRole) {
      const errorResponse = badRequest('role not found');
      return { error: errorResponse, data: null };
    }
    return { error: null, data: existRole };
  } catch (err) {
    const errorResponse = badRequest('role not found');
    return { error: errorResponse, data: null };
  }
};
const createRole = async (
  roleBody: IRawRole
): Promise<{
  error: null | IErrorResponse<IErrorField[]>;
  data: RoleInstance;
}> => {
  if (await getRole(roleBody.name)) {
    const errorResponse = handleErrorsModel({
      key: 'role',
      message: 'Role is taken',
    });
    return { error: errorResponse, data: null };
  }
  return { error: null, data: await Roles.create(roleBody) };
};
const updateAliasRole = async (
  name: string,
  aliasName: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: RoleInstance | null;
}> => {
  const existRole = await getRole(name);
  if (!existRole) {
    const errorResponse = badRequest('role not found');
    return { error: errorResponse, data: null };
  }
  existRole.aliasRole = aliasName;
  await existRole.save();
  return { error: null, data: existRole.toJSON() };
};
const rolesServices = {
  getRoleByName,
  getListRole,
  createRole,
  updateAliasRole,
};
export default rolesServices;
