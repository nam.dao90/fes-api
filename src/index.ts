import configEnv from '@src/config/env';
import { logger } from '@config/logger';
import { Server } from 'http';
import app from './app';
import db from './config/database';
import redisDb from './config/redis';

let server: Server;

db.authenticate()
  .then(() => {
    logger.info('Connected to postgres success');
    server = app.listen(configEnv.port, () => {
      logger.info(`Listening to port ${configEnv.port}`);
    });
    redisDb
      .connect()
      .then(val => {
        logger.info('Connected to redis success', val);
      })
      .catch(error => logger.error(error));
  })
  .catch(error => logger.error(error));
const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error: Error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});
